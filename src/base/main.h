#ifndef MAIN_H
#define MAIN_H

#ifdef NLS
#include <libintl.h>
#include <locale.h>
#endif /* NLS */

#include "bool.h"
#include "string_length.h"
#include "path.h"

#define WINDOW_SIZE_X		800
#define WINDOW_SIZE_Y		600

#ifdef NLS
#define _(text)			gettext(text)
#else /* NLS */
#define _(text)			(text)
#endif /* NLS */

#include "msg.h"

#ifndef __WIN32__
#define PATH_SEPARATOR "/"
#else /* __WIN32__ */
#define PATH_SEPARATOR "\\"
#endif /* __WIN32__ */

extern char *getParam(char *s);
extern char *getParamElse(char *s1, char *s2);
extern bool_t isParamFlag(char *s);
extern char *get_program_name();
extern char *getString(int n);
extern char *get_string_static(int n);
extern int *newInt(int x);
extern int isFillPath(const char *path);
extern void accessExistFile(const char *s);
extern int tryExistFile(const char *s);

#endif /* MAIN_H */
