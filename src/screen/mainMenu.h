#ifndef SCREEN_MAIN_MENU_H
#define SCREEN_MAIN_MENU_H

#include "main.h"

extern void main_menu_start();
extern void main_menu_draw();
extern void main_menu_event();
extern void main_menu_stop();
extern void main_menu_init();
extern void main_menu_quit();

#endif /* SCREEN_MAIN_MENU_H */
