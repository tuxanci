#include <stdlib.h>
#include <assert.h>

#include "main.h"
#include "list.h"

#include "interface.h"
#include "screen.h"
#include "layer.h"
#include "myTimer.h"

/*#define DEBUG_TIME_DRAW*/

static screen_t *currentScreen;
static screen_t *futureScreen;

static list_t *listScreen;

static bool_t isScreenInit = FALSE;

bool_t screen_is_inicialized()
{
	return isScreenInit;
}

screen_t *screen_new(char *name, void (*fce_start) (), void (*fce_event) (),
		     void (*fce_draw) (), void (*fce_stop) ())
{
	screen_t *new;

	assert(name != NULL);
	assert(fce_start != NULL);
	assert(fce_event != NULL);
	assert(fce_draw != NULL);
	assert(fce_stop != NULL);

	new = malloc(sizeof(screen_t));
	new->name = strdup(name);
	new->fce_start = fce_start;
	new->fce_event = fce_event;
	new->fce_draw = fce_draw;
	new->fce_stop = fce_stop;

	return new;
}

void screen_destroy(screen_t *p)
{
	assert(p != NULL);

	free(p->name);
	free(p);
}

void screen_register(screen_t *p)
{
	assert(p != NULL);

	debug("Registering screen [%s]", p->name);

	list_add(listScreen, p);
}

void screen_init()
{
	listScreen = list_new();

	currentScreen = NULL;
	futureScreen = NULL;

	isScreenInit = TRUE;
}

static screen_t *findScreen(char *name)
{
	screen_t *this;
	int i;

	for (i = 0; i < listScreen->count; i++) {
		this = (screen_t *) listScreen->list[i];
		assert(this != NULL);

		if (strcmp(name, this->name) == 0) {
			return this;
		}
	}

	return NULL;
}

void screen_set(char *name)
{
	futureScreen = findScreen(name);
	assert(futureScreen != NULL);
}

void screen_switch()
{
	if (futureScreen == NULL) {
		return;
	}

	layer_flush();

	if (currentScreen != NULL) {
		debug("Stopping screen [%s]", currentScreen->name);
		currentScreen->fce_stop();
	}

	currentScreen = futureScreen;
	futureScreen = NULL;

	debug("Starting screen [%s]", currentScreen->name);

	currentScreen->fce_start();
}

void screen_start(char *name)
{
	screen_set(name);
	screen_switch();
}

char *screen_get()
{
	assert(currentScreen != NULL);
	return currentScreen->name;
}

void screen_draw()
{
	static int count = 0;

	count++;

	if (count == 1) {
		count = 0;

		assert(currentScreen != NULL);

#ifdef DEBUG_TIME_DRAW
		my_time_t prev;

		prev = timer_get_current_timeMicro();
#endif /* DEBUG_TIME_DRAW */

		currentScreen->fce_draw();

		interface_refresh();

#ifdef DEBUG_TIME_DRAW
		printf("c draw time = %d\n", timer_get_current_timeMicro() - prev);
#endif /* DEBUG_TIME_DRAW */
	}
}


void screen_event()
{
	assert(currentScreen != NULL);

	currentScreen->fce_event();

#ifdef DEBUG_TIME_EVENT
	printf("c event time = %d\n", timer_get_current_timeMicro() - prev);
#endif /* DEBUG_TIME_EVENT */
}

void screen_quit()
{
	assert(listScreen != NULL);

	list_destroy_item(listScreen, screen_destroy);
	isScreenInit = FALSE;
}
